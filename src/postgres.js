const { Pool } = require('pg')
const BaseUser = require('./base.js')
const utils = require('./utils.js')


class PostgresUser extends BaseUser {
  constructor(name, options) {
    super(name, options)
    this.options = options
    this.table_info_cache = {}

    this.pool = new Pool({
      user: options.user,
      host: options.host,
      database: options.database,
      password: options.password,
      port: options.port,
      ssl: options.ssl
    })
  }

  handleError(e, res) {
    let err = e.toString();
    console.error(err);
    if (/permission denied/.test(err)) {
      this.forbidden(res)
    } else if (e.detail) {
      this.badRequest(res, err +': ' + e.detail.toString());
    } else if (/column ".*" of relation ".*" does not exist/.test(err)) {
      let matches = err.match(/column "(.*)" of relation "(.*)"/);
      this.badRequest(res, 'column "' + matches[1] + '" in table "'+ matches[2] +'" does not exist');
    } else if (/relation ".*" does not exist/.test(err)) {
      let matches = err.match(/relation "(.*)" does not exist/);
      this.doesNotExist(res, 'table "' + matches[1] + '" does not exit');
    } else {
      this.serverError(res, err);
    }
  }

  serverError(res, msg) {
    res.status(500).json({error: `server error: ${msg}`});
  }

  doesNotExist(res, msg) {
    res.status(404).json({error: `does not exist: ${msg}`});
  }

  getFeatures(dbRes, info) {
    const pk = (info ? info.pk : undefined );
    const geom = (info ? info.geom : undefined);

    let features = dbRes.rows.map((row) =>
      this.rowToFeature(row, pk, geom))

    return features;
  }

  async query(req, res, sql, params, collection) {
    sql = sql.replace('{table}', req.table);
    console.log(sql, params);

    let dbRes;
    try {
      dbRes = await this.pool.query(sql, params);
    } catch (e) {
      this.handleError(e, res);
      return;
    }

    let info;
    try {
      info = await this.getTableInfo(req);
    } catch (e) {
      this.handleError(e, res);
      return;
    }
    
    let features;
    try {
      const pk = (info && info.pk ? info.pk : undefined );
      const geom = (info && info.geom ? info.geom : undefined);

      features = dbRes.rows.map((row) =>
        this.rowToFeature(row, pk, geom))
    } catch (e) {
      this.handleError(e, res);
      return;
    }

    try {
      if (dbRes.command === 'INSERT') {
        res.json(features[0]);
      } else if (dbRes.command === 'SELECT') {
        if (collection) {
          res.json({
            type: 'FeatureCollection',
            features: features
          })
        } else {
          res.json(features[0])
        }
      } else if (dbRes.command === 'UPDATE') {
        res.json(features[0]);
      }
    } catch (e) {
      this.handleError(e, res);
      return;
    }
  }

  async getTableInfo(req) {
    let info = this.table_info_cache[req.table]
    if (info !== undefined) return info

    let tableParts = req.table.split('.', 2)
    tableParts.reverse()

    let pk = this.options.defaultPk || 'id'
    let geom = this.options.defaultGeom || 'geom'

    info = {
      created: false,
      geom: null,
      insert: false,
      ip: false,
      name: req.table,
      modified: false,
      pk: null,
      schema: tableParts[1] || 'public',
      select: false,
      srid: this.options.defaultSrid || null,
      table: tableParts[0],
      update: false
    }

    let pkSql = `SELECT
      "c"."column_name"
    FROM "information_schema"."table_constraints" "t"
    LEFT JOIN "information_schema"."key_column_usage" "c"
      ON "c"."constraint_name" = "t"."constraint_name"
    WHERE
      "c"."table_schema" = $2
      AND "t"."table_schema" = $2
      AND "c"."table_name" = $1
      AND constraint_type = 'PRIMARY KEY'`

    let colSql = `SELECT column_name, data_type
    FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = $1
    AND table_schema = $2`

    let permSql = `SELECT privilege_type
    FROM information_schema.role_table_grants
    WHERE table_name = $1 AND table_schema = $2 AND grantee = $3`


    let [colRes, permRes, pkRes] = await Promise.all([
      this.pool.query(colSql, [info.table, info.schema]),
      this.pool.query(permSql, [info.table, info.schema, this.options.user]),
      this.pool.query(pkSql, [info.table, info.schema])
    ])

    // if (colRes.rows.length === 0) {
    //   // The user does not have access to the table.
    //   this.table_info_cache[req.table] = null
    //   return null
    // }

    if (pkRes.rows.length !== 0) {
      if (pkRes.rows.length !== 1) {
        // pkSql should only return 1 row, which is the primary key join.
        throw new Error("Primary Key SQL returned more than one row");
      } else {
        info.pk = pkRes.rows[0].column_name;
      }
    }

    colRes.rows.forEach((row) => {
      if (row.column_name === '_created' &&
          /timestamp/.test(row.data_type)) {
	      info.created = true
      } else if (row.column_name === '_modified' &&
                 /timestamp/.test(row.data_type)) {
	      info.modified = true
      } else if (row.column_name === '_ip' &&
                 /character varying/.test(row.data_type)) {
	      info.ip = true
      } else if (row.column_name === pk) {
        info.pk = pk
      } else if (row.column_name === geom) {
        info.geom = geom
      }
    })

    if (permRes.rows.length === 0) {
      // If no permissions are explicitly granted, we assume the user is the
      // table owner and has all permissions.
      info.insert = true
      info.select = true
      info.update = true
    } else {
      permRes.rows.forEach((row) => {
        if (row.privilege_type === 'INSERT') info.insert = true
        if (row.privilege_type === 'SELECT') info.select = true
        if (row.privilege_type === 'UPDATE') info.update = true
      })
    }

    this.table_info_cache[req.table] = info
    return info
  }

  rowToFeature(row, pk, geom) {
    let feature = {
      type: 'Feature',
      properties: {...row}
    }

    if (pk) {
      feature.id = row[pk]
      delete feature.properties[pk]
    }

    if (geom && feature.properties[geom]) {
      feature.geometry = JSON.parse(feature.properties[geom])
      delete feature.properties[geom]
    }

    return feature
  }

  getFeatureInfo(req) {
    let info = req.tableInfo
    let feature = req.body
    let insert = req.method === 'POST'

    let names = []
    let placeholders = []
    let values = []
    let i = 1
    let now = new Date()

    if (feature.properties) for (let name in feature.properties) {
      names.push(utils.sqlSanitize(name))
      placeholders.push('$' + i++)
      values.push(feature.properties[name])
    }

    if (info.geom && feature.geometry) {
      let geom = 'ST_SetSRID(ST_GeomFromGeoJSON($' + i++ + '), 4326)'
      names.push(info.geom)
      placeholders.push(
        (info.srid) ? `ST_Transform(${geom}, ${info.srid})` : geom)
      values.push(JSON.stringify(feature.geometry))
    }

    if (info.ip && insert) {
      names.push('_ip')
      placeholders.push('$' + i++)
      values.push(req.headers['x-forwarded-for'] || req.connection.remoteAddress)
    }

    if (info.created && insert) {
      names.push('_created')
      placeholders.push('$' + i++)
      values.push(now)
    }

    if (info.modified) {
      names.push('_modified')
      placeholders.push('$' + i++)
      values.push(now)
    }

    return {names, placeholders, values}
  }

  async middleware(req, res, next) {
    try {
      req.tableInfo = await this.getTableInfo(req)
    } catch (e) {
      this.handleError(e, res)
      return
    }
    if (req.tableInfo === null) return this.forbidden(res)

    if (req.body) req.featureInfo = this.getFeatureInfo(req)

    return next()
  }

  async getTable(req, res) {
    let info = req.tableInfo
    if (!info.select) return this.forbidden(res)
    let sql = 'SELECT *'
    if (info.geom)
      sql += `, ST_AsGeoJSON(ST_Transform(${info.geom}, 4326)) AS ${info.geom}`
    sql += ' FROM {table}'
    return this.query(req, res, sql, undefined, true)
  }

  async postFeature(req, res) {
    let {names, placeholders, values} = req.featureInfo
    if (!req.tableInfo.insert) return this.forbidden(res)
    let sql = `INSERT INTO ${req.table} (${names.toString()}) ` +
        `VALUES (${placeholders.toString()})`
    if (req.tableInfo.select) sql += `RETURNING ${req.tableInfo.pk}`
    console.log(req.tableInfo);
    return this.query(req, res, sql, values)
  }

  getFeature(req, res) {
    let info = req.tableInfo
    if (!info.select) return this.forbidden(res)
    let sql = 'SELECT *'
    if (info.geom)
      sql += `, ST_AsGeoJSON(ST_Transform(${info.geom}, 4326)) AS ${info.geom}`
    sql += ` FROM {table} WHERE ${utils.sqlSanitize(info.pk)} = $1`
    return this.query(req, res, sql, [req.fid], false)
  }

  patchFeature(req, res) {
    let info = req.tableInfo;
    if (!info.update) { return this.forbidden(res); }
    let {names, placeholders, values} = req.featureInfo;
    if (names.length !== placeholders.length || placeholders.length !== values.length) {
      console.error("The number of names and the number of values is unequal");
      this.badRequest(res, "Misformed properties, number of properties does not match number of values");
    }
    let sql = `UPDATE ${info.schema}.${info.table} SET `;
    names.forEach((name, index) => {
      let placeholder = placeholders[index];
      sql += `${name}=${placeholder}`;
      if (index !== names.length - 1) {
        sql += ', ';
      } else {
        sql += ' ';
      }
    });
    sql += `WHERE ${info.pk} = $${placeholders.length + 1} `;
    sql += `RETURNING ${info.pk}`;

    return this.query(req, res, sql, [...values, req.fid], false);
  }

  deleteFeature(req, res) {
    this.doesNotExist(res, 'Delete feature not implemented')
  }
}

module.exports = PostgresUser
