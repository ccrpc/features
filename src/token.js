const fs = require('fs')
const jwt = require('jsonwebtoken')
const { promisify } = require('util')

async function getConfig(filename, user) {
  let configFile = await promisify(fs.readFile)(filename)
  return JSON.parse(configFile).users[user]
}

function generateToken(user, duration, config) {
  let claims = {
    sub: user,
    exp: Math.ceil((new Date()).getTime() / 1000) + duration
  }
  if (config.issuer) claims.iss = config.issuer
  if (config.audience) claims.aud = config.audience

  let options = {
    algorithm: 'HS256',
  }

  return new Promise((resolve, reject) => {
    jwt.sign(claims, config.secret, options, (err, token) => {
      (err) ? reject(err) : resolve(token)
    })
  })
}

async function getToken(user, duration, auth) {
  let token = await generateToken(user, duration, auth)
  console.log(token)
}

async function getInfo(user, token, auth) {
  jwt.verify(token, auth.secret, {
    algorithms: ['HS256'],
    audience: auth.audience,
    issuer: auth.issuer
  }, (err, decoded) => {
    if (err) {
      console.log('Invalid token: ' + err.message)
    } else if (decoded.sub !== user) {
      console.log(`Token subject ${decoded.sub} does not match user ${user}`)
    } else {
      console.log('User: ' + decoded.sub)
      console.log('Issuer: ' + decoded.iss)
      console.log('Issued: ' + (new Date(decoded.iat*1000)).toLocaleString())
      console.log('Expires: ' + (new Date(decoded.exp*1000)).toLocaleString())
    }
  })
}

async function run() {
  let command = process.argv[2]
  let user = process.argv[3]
  let durationOrToken = process.argv[4]
  let configFilename = process.argv[5] || 'config.json'
  let config = await getConfig(configFilename, user)

  if (command === 'generate') {
    getToken(user, parseInt(durationOrToken || '86400'), config.auth)
  } else if (command === 'info') {
    getInfo(user, durationOrToken, config.auth)
  } else {
    console.error('Invalid command')
  }
}

if (require.main === module) run()
