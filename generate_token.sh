#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "usage: get_token [user] [duration]"
    echo "example: get_token public 220752000"
    return 1
  fi

docker build -t features:sh_token .
docker create -p 8080:8080 --name features_sh_token features:sh_token yarn start /usr/src/app/config.json
docker cp config.json features_sh_token:/usr/src/app/config.json
docker start features_sh_token

docker exec features_sh_token yarn run --silent token generate $1 $2 | xclip

echo "Authorization: Bearer $(xclip -o)"

docker stop features_sh_token
docker container rm features_sh_token
docker image rm features:sh_token