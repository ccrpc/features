FROM node:9-alpine

WORKDIR /usr/src/app

COPY package.json yarn.lock /usr/src/app/
RUN yarn install --frozen-lockfile
COPY ./src /usr/src/app/src

# COPY config.json /etc/features

EXPOSE 8080
CMD [ "yarn", "start", "/etc/features/config.json" ]
