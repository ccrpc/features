# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/ccrpc/features/compare/v0.1.1...v0.1.2) (2022-09-14)

### Features

* Updating to v0.1.2 ([e0a065b](https://gitlab.com/ccrpc/features/commit/e0a065ba6d2730ed601c94ced7a77f794c42a047))

### 0.1.1 (2020-09-10)
