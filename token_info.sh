#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "usage: get_token [user] [token]"
    echo "example: get_token public [not going to put an actual token here sorry]"
    return 1
  fi

docker build -t features:token_info .
docker create -p 8080:8080 --name features_token_info features:token_info yarn start /usr/src/app/config.json
docker cp config.json features_token_info:/usr/src/app/config.json
docker start features_token_info

docker exec features_token_info yarn run --silent token info $1 $2 | xclip

echo "Token Info:"
echo "$(xclip -o)"

docker stop features_token_info
docker container rm features_token_info
docker image rm features:token_info